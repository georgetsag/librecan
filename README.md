LibreCAN takes the group of operations you need to execute in order to work over CAN bus to an abstract layer and provides you with an easy to use interface.

#### As an early version, LibreCAN imposes a few restrictions:    

    Device network can have a maximum of 8 devices, each given a unique ID, DEVICE_0 to DEVICE_7.
    Each device can be given a maximum of 32 different message IDs.
    
#### Setting up LibreCAN requires the call of a setup-function, which requires two parameters:    

    1.The device id you wish to give the device.    
    2.A standard procedure you are required to define, which is the system-dependent procedure for transmitting 8bytes over CAN bus with a given StdId.
    LibreCAN requires a function of type (void)(uint8_t StdId, uint8_t data[8]), which should contain the system's procedure for transmitting data[] with StdId on CAN bus.   

#### Sending messages with LibreCAN:

    After setting up correctly, you are ready to use the TX function.
    LibreCAN uses the can_message_t struct, which is later passed into LibreCAN_sendMessage(can_message_t) for executing the TX operation.
    After populating can_message_t fields, (uint8_t[8])data and (uint8_t)message_id, you can pass it to LibreCAN_sendMessage(can_message_t) to execute the operation.

#### Receiving messages with LibreCAN:    

    When your system receives a message over CAN bus, it invokes a function / handler for it.
    Call the LibreCAN data RX handler from within the system handler by invoking LibreCAN_Data_Rx_handler(uint8_t StdId, uint8_t data[8]) and passing the arguments respectively.
    
#### Defining handlers for messages:    

    LibreCAN supports the definition of handlers for different messages received by any device.
    To set a handler for a message received by a device call LibreCAN_setHandler(device_id_t broadcaster, uint8_t message_id, void (*handler)(can_message_t message)).
    Handler function's prototype is (void)(can_message_t), which should contain the desired actions for handling the message with message_id received from (device_id_t)broadcaster. 
    
    

    