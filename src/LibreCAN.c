#include "LibreCAN.h"
#include "main.h"

void setupLibreCAN(device_id_t device_id, void (*TxProcedure)(uint8_t StdId, uint8_t data[8])){
    this_device_id = device_id;
    LibreCAN_setTxProcedure(TxProcedure);
}

void initLibreCAN(){
	LibreCAN_device_initDiscoverageTable();
}

/**
 * 
 * TX FUNCTIONS
 * 
**/

void LibreCAN_sendMessage(can_message_t message){
    LibreCAN_Tx_Procedure(LibreCAN_createCanStdId(message.message_id), message.data);
}

void LibreCAN_sendFrame(uint8_t StdId, uint8_t data[8]){
	can_message_t message = LibreCAN_getMessage(StdId, data);
	LibreCAN_sendMessage(message);
}

/**
 * 
 * TX FUNCTIONS END
 * 
**/

/**
 * 
 * RX FUNCTIONS
 * 
**/

void LibreCAN_Data_Rx_handler(uint8_t StdId, uint8_t data[8]){
	can_message_t message = LibreCAN_getMessage(StdId, data);
	LibreCAN_MessageRxCenter(message);
}

/* RX CENTER */
void LibreCAN_MessageRxCenter(can_message_t message){
	LibreCAN_Rx_handlers[message.broadcaster][message.message_id](message);
}
/* RX CENTER END */

void LibreCAN_Rx_Frame(uint8_t std_id, uint8_t data[8]){
	can_message_t message;
	message = LibreCAN_getMessage(std_id, data);
	LibreCAN_MessageRxCenter(message);
}

/**
 * 
 * RX FUNCTIONS END
 * 
**/
