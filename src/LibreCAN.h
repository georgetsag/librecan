#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef LIBRE_CAN_INCLUDED
#define LIBRE_CAN_INCLUDED

#include "device.h"
#include "message.h"
#include "c_utilities.h"
#include "CAN_utilities.h"

void initLibreCAN();

void setupLibreCAN(device_id_t device_id, void (*TxProcedure)(uint8_t StdId, uint8_t data[8]));

void LibreCAN_sendMessage(can_message_t message);

void LibreCAN_sendFrame(uint8_t StdId, uint8_t data[8]);

void (*LibreCAN_Tx_Procedure)(uint8_t, uint8_t[8]);

void LibreCAN_MessageRxCenter(can_message_t message);

void LibreCAN_Data_Rx_handler(uint8_t StdId, uint8_t data[8]);

void LibreCAN_Rx_Frame(uint8_t std_id, uint8_t data[8]);

#endif //LIBRE_CAN_INCLUDED
