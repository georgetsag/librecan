#include "CAN_utilities.h"

uint8_t LibreCAN_createCanStdId(uint8_t message_id){
    return (this_device_id << 5) | (message_id & 31);
}

void LibreCAN_setHandler(device_id_t broadcaster, uint8_t message_id, void (*handler)(can_message_t message)){
	LibreCAN_Rx_handlers[broadcaster][message_id] = handler;
}

void LibreCAN_setTxProcedure(void (*TxProcedure)(uint8_t StdId, uint8_t data[8])){
	LibreCAN_Tx_Procedure = TxProcedure;
}
