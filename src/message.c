#include "message.h"

can_message_t LibreCAN_getMessage(uint8_t StdId, uint8_t data[8]){
	can_message_t message;
	memcpy(message.data, data, sizeof(&data[0]));
	message.message_id = StdId & 31;
	message.broadcaster = StdId & 224;
	return message;
}

can_message_t LibreCAN_createMessage(uint8_t message_id, uint8_t data[8]){
	can_message_t message;
	message.message_id = message_id;
	message.broadcaster = this_device_id;
	memcpy(message.data, data, sizeof(message.data));
	return message;
}
