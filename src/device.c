#include "device.h"

void LibreCAN_device_initDiscoverageTable(){
	for(uint8_t i = 0; i < 8; i++)
		discoverage_table[i] = 0;
}

void LibreCAN_device_sendDiscoverageMessage(){
	can_message_t discoverage_message;
	discoverage_message.message_id = MSG_ID_DISCOVERAGE;
	memset(discoverage_message.data, 0, sizeof(discoverage_message.data));
	LibreCAN_sendMessage(discoverage_message);
}

void LibreCAN_device_discoverDevice(device_id_t device_id){
	discoverage_table[device_id] = 1;
}

bool LibreCAN_device_isDiscovered(device_id_t device_id){
	return discoverage_table[device_id];
}
