#include "LibreCAN.h"

#ifndef CAN_UTILITIES_INCLUDED
#define CAN_UTILITIES_INCLUDED

uint8_t LibreCAN_createCanStdId(uint8_t message_id);

void (*LibreCAN_Rx_handlers[8][32])(can_message_t);

void LibreCAN_setHandler(device_id_t broadcaster, uint8_t message_id, void (*handler)(can_message_t message));

void LibreCAN_setTxProcedure(void (*TxProcedure)(uint8_t StdId, uint8_t data[8]));

#endif //CAN_UTILITIES_INCLUDED
