#include "LibreCAN.h"

#ifndef MESSAGE_INCLUDED
#define MESSAGE_INCLUDED

typedef struct {
    uint8_t data[8];
    unsigned int message_id : 5;
    device_id_t broadcaster;
} can_message_t;

typedef enum std_message_id_t {
	MSG_ID_DISCOVERAGE = 31
} std_message_id_t;

can_message_t LibreCAN_getMessage(uint8_t StdId, uint8_t data[8]);

can_message_t LibreCAN_createMessage(uint8_t message_id, uint8_t data[8]);

#endif //MESSAGE_INCLUDED
