#include "LibreCAN.h"

#ifndef DEVICE_INCLUDED
#define DEVICE_INCLUDED

typedef enum device_id_t {
    DEVICE_0 = 0,
    DEVICE_1,
    DEVICE_2,
    DEVICE_3,
    DEVICE_4,
    DEVICE_5,
    DEVICE_6,
    DEVICE_7
} device_id_t;

void LibreCAN_device_initDiscoverageTable();
void LibreCAN_device_sendDiscoverageMessage();
void LibreCAN_device_discoverDevice(device_id_t device_id);

device_id_t this_device_id;
bool discoverage_table[8];

#endif //DEVICE_INCLUDED
